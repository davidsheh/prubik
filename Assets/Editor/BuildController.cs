﻿using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;

public class BuildController : IPreprocessBuild, IPostprocessBuild
{
    public int callbackOrder
    {
        get
        {
            return 1;
        }
    }

    void IPreprocessBuild.OnPreprocessBuild(BuildTarget target, string path)
    {
        if (target == BuildTarget.iOS)
        {

        }
        else if (target == BuildTarget.Android)
        {
            // 现在市面上的很多手机都没有内存卡，如果不强制 ForceInternal 的话，启动游戏可能会导致崩溃
            PlayerSettings.Android.preferredInstallLocation = AndroidPreferredInstallLocation.ForceInternal;
            //ModifyAndroidManifest();

            SetKeystore();
        }
    }

    void IPostprocessBuild.OnPostprocessBuild(BuildTarget target, string path)
    {
        if (target == BuildTarget.iOS)
        {
            Debug.Log("iOS build successfully!");
        }
        else if (target == BuildTarget.Android)
        {
            Debug.Log("Android build successfully!");
        }
    }

    public static void ModifyAndroidManifest()
    {
        string path = "Assets/Plugins/Android/GoogleMobileAdsPlugin/AndroidManifest.xml";
        string appId = "";
#if EM_ADMOB
        var mAdSettings = EasyMobile.EM_Settings.Advertising.AdMob;
        appId = mAdSettings.AppId.AndroidId;
#endif

        var manifest = AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset)) as TextAsset;
        string txt = manifest.text;
        string tag = "[ADMOB APPLICATION ID]";
        if (txt.Contains(tag))
        {
            txt = txt.Replace(tag, appId);
        }
        else if (txt.Contains("android:value=\"ca-app-pub-"))
        {
            string pattern = "ca-app-pub-[0-9]+~[0-9]+";
            txt = System.Text.RegularExpressions.Regex.Replace(txt, pattern, appId);
        }
        File.WriteAllText(path, txt);
        EditorUtility.SetDirty(manifest);

        Debug.Log("Android manifest appId is replaced successfully! App id is " + appId);
    }

    public static void SetKeystore()
    {
        if (!File.Exists(Consts.KeystoreName))
        {
            CreateKeystore();
        }

        // keystore 文件路径
        PlayerSettings.Android.keystoreName = Consts.KeystoreName;

        // 密钥密码
        PlayerSettings.Android.keystorePass = Consts.KeystorePass;

        // 密钥别名
        PlayerSettings.Android.keyaliasName = Consts.KeyaliasName;

        // 密码，默认是 keystorePass@keyaliasName
        PlayerSettings.Android.keyaliasPass = Consts.KeyaliasPass;
    }

    public static void CreateKeystore()
    {
        if (!Directory.Exists(Consts.KeystorePath))
        {
            Directory.CreateDirectory(Consts.KeystorePath);
        }

        /*
        CN  -->  First and Last Name        
        OU  -->  Oraganizational Unit       
        O   -->  Organization               
        L   -->  City or Locality           
        ST  -->  State or Province          
        C   -->  Country Code  
        */
        string dname = string.Format("-dname \"C=CN, ST=China, L=Beijing, O={0}, OU={0}, CN={0}\"", Consts.CompanyName);
        string cmd = string.Format("keytool -genkey -v -keystore {0} -alias {1} -storepass {2} -keypass {3} -keyalg RSA -validity 36500 {4}", Consts.KeystoreName, Consts.KeyaliasName, Consts.KeystorePass, Consts.KeyaliasPass, dname);

        Debug.Log("gen keystore ------ " + cmd);

        ShellHelper.ShellRequest req = ShellHelper.ProcessCommand(cmd, "");
        req.onLog += delegate (int arg1, string arg2)
        {
            Debug.Log(arg2);
        };
    }
}
