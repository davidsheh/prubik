﻿using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class AutoEditorSettings
{
    static AutoEditorSettings()
    {
        EditorSettings.externalVersionControl = "Visible Meta Files";
        EditorSettings.serializationMode = SerializationMode.ForceText;

        PlayerSettings.companyName = Consts.CompanyName;
        PlayerSettings.productName = Consts.GameName;
        PlayerSettings.applicationIdentifier = Consts.PackageName;

        Debug.Log(string.Format("CompanyName: {0}, GameName: {1}, PackageName: {2}", Consts.CompanyName, Consts.GameName, Consts.PackageName));
    }
}
