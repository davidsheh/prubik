﻿using UnityEditor;
using UnityEngine;

public class ToolsMenu
{
    //[MenuItem("Tools/Build Andorid Apk")]
    //public static void BuildApk()
    //{
        
    //}

#if UNITY_ANDROID
    [MenuItem("Tools/获取 Android SHA1 签名")]
    public static void SHA1()
    {
        if (!System.IO.File.Exists(Consts.KeystoreName))
        {
            if (EditorUtility.DisplayDialog("签名文件不存在", "是否先创建签名文件？创建完成后请重新获取 SHA1 签名。", "确定", "取消"))
            {
                BuildController.CreateKeystore();
            }
            
            return;
        }

        string cmd = string.Format("keytool -list -v -keystore {0} -alias {1} -storepass {2} -keypass {3}", Consts.KeystoreName, Consts.KeyaliasName, Consts.KeystorePass, Consts.KeyaliasPass);

        Debug.Log("SHA1 ---------- " + cmd);

        ShellHelper.ShellRequest req = ShellHelper.ProcessCommand(cmd, "");
        req.onLog += delegate (int arg1, string arg2) {
            string flag = "SHA1: ";
            if (arg2.Contains(flag))
            {
                string output = arg2.Replace(flag, string.Empty).Trim();
                Debug.Log(output);

                GUIUtility.systemCopyBuffer = output;

                EditorUtility.DisplayDialog("Android 签名成功", "SHA1 签名已经复制到粘贴板，\n请前往 Google Play Console 粘贴！", "确定");
            }
        };
    }

    [MenuItem("Tools/修改 AndroidManifest 文件")]
    public static void ModifyAndroidManifest()
    {
        BuildController.ModifyAndroidManifest();
    }

    //[MenuItem("Tools/输出包名")]
    //public static void OutputPackageName()
    //{
    //    AutoEditorSettings.SetPackageName();
    //}
#endif
}
