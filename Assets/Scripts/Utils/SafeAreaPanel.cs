﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 设计安全区域面板（适配iPhone X）
/// Jeff 2017-12-1
/// 文件名 SafeAreaPanel.cs
/// </summary>
public class SafeAreaPanel : MonoBehaviour
{

    private RectTransform target;

#if UNITY_EDITOR
    [SerializeField]
    private bool Simulate_X = false;
#endif


    void Awake()
    {
        target = GetComponent<RectTransform>();
    }

    Rect lastSafeArea = new Rect(0, 0, 0, 0);
    void ApplySafeArea(Rect area)
    {
        #if UNITY_EDITOR
        float Margin = 44f;

        if (Simulate_X)
        {
			float insets = 0;
			var positionOffset = Vector2.zero;
			var sizeOffset = Vector2.zero;
			if(Screen.width > Screen.height)
            { 
				insets = area.width * Margin / Screen.width;
				positionOffset = new Vector2(insets, 0);
            	sizeOffset = new Vector2(insets * 2, 0);
			}
			else
			{
				insets = area.height * Margin / Screen.height;
				positionOffset = new Vector2(0, insets);
            	sizeOffset = new Vector2(0, insets * 2);
			}
            
            area.position = area.position + positionOffset;
            area.size = area.size - sizeOffset;
        }
        #endif

        var anchorMin = area.position;
        var anchorMax = area.position + area.size;
        anchorMin.x /= Screen.width;
        anchorMin.y /= Screen.height;
        anchorMax.x /= Screen.width;
        anchorMax.y /= Screen.height;
        target.anchorMin = anchorMin;
        target.anchorMax = anchorMax;

        lastSafeArea = area;
    }

    // Update is called once per frame
    void Update () 
    {
        Rect safeArea = Screen.safeArea;

        if (safeArea != lastSafeArea)
            ApplySafeArea(safeArea);
    }
}