﻿#if UNITY_EDITOR
using UnityEngine;
using System.IO;

public class ScreenshotNow : MonoBehaviour
{
    public string name;
    //定义图片保存路径  
    private string mPath1;
    private string mPath2;
    private string mPath3;

    //相机  
    public Transform CameraTrans;

    private static int number = 0;
    void Start()
    {

    }

    //主方法，使用UGUI实现  
    void OnGUI()
    {
        if (Input.GetKeyUp(KeyCode.A))
        {
            CaptureByUnity();
        }
    }

    private void CaptureByUnity()
    {
        string path = Application.dataPath + "/../Screenshots";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        //初始化路径  
        path += "/Screenshot_{0}_{1}.png";
        name = UnityEditor.PlayerSettings.productName;
        var mFileName = string.Format(path, name, number);
        ScreenCapture.CaptureScreenshot(mFileName, 0);
        number++;
    }
}
#endif