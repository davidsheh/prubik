﻿using UnityEngine;

public class Main : MonoBehaviour
{
    void Awake()
    {
        var objGlobal = GameObject.Find("GlobalManager");
        if (null == objGlobal)
        {
            new GameObject("GlobalManager", typeof(GlobalManager));
        }
    }
}