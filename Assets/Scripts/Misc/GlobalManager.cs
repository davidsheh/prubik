using UnityEngine;
using EasyMobile;
using UnityEngine.SceneManagement;
using System.Collections;

public class GlobalManager : MonoBehaviour
{
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        gameObject.AddComponent<AdManager>();
        gameObject.AddComponent<GameServiceManager>();

#if UNITY_EDITOR
        gameObject.AddComponent<ScreenshotNow>();
#endif
        gameObject.AddComponent<Gracecat.CrossPromation>();

        SceneManager.activeSceneChanged += OnActiveSceneChanged;
    }

    private readonly WaitForSeconds wfs = new WaitForSeconds(1.0f);
    IEnumerator Start()
    {
        // var installTime = RuntimeHelper.GetAppInstallationTime();
        AdManager.Instance.ShowBanner(BannerAdPosition.Bottom);

        while (true)
        {
            yield return wfs;

            if (Gracecat.CrossPromation.Instance.IsReady())
            {
                Gracecat.CrossPromation.Instance.ShowRecommandApp();
                yield break;
            }
        }
        // PlayerPrefs.DeleteAll();
    }

    void OnActiveSceneChanged(Scene current, Scene next)
    {
        Debug.Log(current.name + "," + next.name);

        string currentName = current.name;

        if (currentName == null)
        {
            currentName = "Replaced";
        }
        // TODO
    }

    void OnDestroy()
    {
        SceneManager.activeSceneChanged -= OnActiveSceneChanged;
    }

    void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Gracecat.CrossPromation.Instance.HideRecommandApp();
            // Ask if user wants to exit
            NativeUI.AlertPopup alert = NativeUI.ShowTwoButtonAlert("Exit App",
                                            "Do you want to exit?",
                                            "Yes",
                                            "No");

            if (alert != null)
                alert.OnComplete += delegate (int button)
                {
                    if (button == 0)
                        Application.Quit();
                };
        }
#endif
    }
}
