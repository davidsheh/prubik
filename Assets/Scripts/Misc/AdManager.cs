﻿using UnityEngine;
using EasyMobile;
using System;

public class AdManager : MonoBehaviour
{
    public static AdManager Instance = null;

    private Action<bool> OnReward = null;

    void Awake()
    {
        if (null == Instance)
        {
            Instance = this;
        }

        // Init EM runtime if needed (useful in case only this scene is built).
        if (!RuntimeManager.IsInitialized())
            RuntimeManager.Init();
    }

    private void Start()
    {
    }

    #region Banner
    public virtual void ShowBanner(BannerAdPosition adPos = BannerAdPosition.Bottom)
    {
        if (Advertising.IsAdRemoved())
        {
            NativeUI.Alert("Alert", "Ads were removed.");
            return;
        }

        Advertising.ShowBannerAd(adPos, BannerAdSize.SmartBanner);
    }

    public virtual void HideBanner()
    {
        Advertising.HideBannerAd();
    }

    public virtual void DestroyBanner()
    {
        Advertising.DestroyBannerAd();
    }
    #endregion

    #region Interstitial
    public void ShowInterstitialAd()
    {
        if (Advertising.IsInterstitialAdReady())
        {
            Advertising.ShowInterstitialAd();
        }
    }
    #endregion

    #region Reward
    public void ShowRewardAd(Action<bool> callback = null)
    {
        if (null != callback)
        {
            OnReward = callback;
        }
        if (Advertising.IsRewardedAdReady())
        {
            Advertising.ShowRewardedAd();
        }
        else
        {
            NativeUI.AlertPopup alert = NativeUI.Alert("Alert", "Video is not ready yet, hold on!");
            if (alert != null)
            {
                alert.OnComplete += delegate (int button)
                {
                    if (null != callback)
                    {
                        callback(false);
                    }
                };
            }
        }
    }
    #endregion

    private float lastTime = 0;
    private const float intervalTime = 50f; // 50秒一次广告
    private const int rewardRate = 3;
    private static int AdShowTimes = 0;
    public void ShowAd()
    {
        var time = Time.realtimeSinceStartup;
        if (time - lastTime < intervalTime)
        {
            return;
        }

        lastTime = time;
        AdShowTimes++;
        // if(0 == AdShowTimes % rewardRate)
        // {
        //     Debug.Log("Ad:ShowReward--------------");
        // 	ShowRewardAd();
        // }
        // else
        {
            Debug.Log("Ad:ShowInterstitial--------------");
            ShowInterstitialAd();
        }
    }

    /// <summary>
    /// Removes the ads.
    /// </summary>
    public void RemoveAds()
    {
        Advertising.RemoveAds();
        //NativeUI.Alert("Alert", "Ads were removed. Future ads won't be shown except rewarded ads.");
    }

    /// <summary>
    /// Resets the remove ads.
    /// </summary>
    public void ResetRemoveAds()
    {
        Advertising.ResetRemoveAds();
        //NativeUI.Alert("Alert", "Remove Ads status was reset. Ads will be shown normally.");
    }

    #region Events callback

    private void OnEnable()
    {
        Advertising.RewardedAdSkipped += OnRewardedAdSkipped;
        Advertising.RewardedAdCompleted += OnRewardedAdCompleted;
        Advertising.InterstitialAdCompleted += OnInterstitialAdCompleted;
    }

    private void OnDisable()
    {
        Advertising.RewardedAdSkipped -= OnRewardedAdSkipped;
        Advertising.RewardedAdCompleted -= OnRewardedAdCompleted;
        Advertising.InterstitialAdCompleted -= OnInterstitialAdCompleted;
    }

    private void OnInterstitialAdCompleted(InterstitialAdNetwork network, AdPlacement placement)
    {
        Debug.Log(string.Format(
            "Interstitial ad has been closed. Network: {0}, Placement: {1}",
            network, AdPlacement.GetPrintableName(placement)));
    }

    private void OnRewardedAdCompleted(RewardedAdNetwork network, AdPlacement placement)
    {
        Debug.Log(string.Format(
            "The rewarded ad has completed, this is when you should reward the user. Network: {0}, Placement: {1}",
            network, AdPlacement.GetPrintableName(placement)));

        if (null != OnReward)
        {
            OnReward(true);
            OnReward = null;
        }
    }

    private void OnRewardedAdSkipped(RewardedAdNetwork network, AdPlacement placement)
    {
        Debug.Log(string.Format(
            "The rewarded ad was skipped, and the user shouldn't get the reward. Network: {0}, Placement: {1}",
            network, AdPlacement.GetPrintableName(placement)));

        if (null != OnReward)
        {
            OnReward(false);
            OnReward = null;
        }
    }
    #endregion
}