﻿//#define GIZMOS
//#define TEST

using System;
using System.Collections;
using System.Collections.Generic;
using Unity.UIWidgets.animation;
using Unity.UIWidgets.engine;
using Unity.UIWidgets.foundation;
using Unity.UIWidgets.gestures;
using Unity.UIWidgets.material;
using Unity.UIWidgets.painting;
using Unity.UIWidgets.rendering;
using Unity.UIWidgets.ui;
using Unity.UIWidgets.widgets;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using TextStyle = Unity.UIWidgets.painting.TextStyle;

namespace Gracecat
{
    public class CrossPromation : MonoBehaviour
    {
        public static CrossPromation Instance;
        private string json = string.Empty;
        private CrossPromationWidget widget = null;

        void Awake()
        {
            if (null == Instance)
            {
                Instance = this;
            }
        }

        public void ShowRecommandApp()
        {
            return;
            if (null == this.widget)
            {
                this.widget = CreateWidget();
            }
            
            if (this.widget && !string.IsNullOrEmpty(json))
            {
                widget.enabled = true;
            }
        }

        public void HideRecommandApp()
        {
            if (null != this.widget)
            {
                widget.enabled = false;
            }
        }

        public bool IsReady()
        {
            return !string.IsNullOrEmpty(json);
        }

        void Start()
        {
            StartCoroutine(GetText((apps) => {
                CrossPromationWidget.apps = apps;
                //var app = panel.AddComponent<CrossPromationWidget>();
                //var shader = Shader.Find("UIWidgets/UIDefault");
                //if (shader == null)
                //{
                //    throw new Exception("UIWidgets/UIDefault not found");
                //}
                //var mat = new UnityEngine.Material(shader)
                //{
                //    hideFlags = HideFlags.HideAndDontSave
                //};
                //app.material = mat;
            }));
        }

        private CrossPromationWidget CreateWidget()
        {
            var canvas = GameObject.FindObjectOfType<UnityEngine.Canvas>();
            if (null == canvas || canvas.renderMode != RenderMode.ScreenSpaceOverlay)
            {
                if(null == canvas)
                {
                    new GameObject("EventSystem", typeof(EventSystem), typeof(StandaloneInputModule));
                }
                var objCanvas = new GameObject("Canvas");
                canvas = objCanvas.AddComponent<UnityEngine.Canvas>();
                objCanvas.AddComponent<UnityEngine.UI.CanvasScaler>();
                objCanvas.AddComponent<UnityEngine.UI.GraphicRaycaster>();
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            }
            var cpw = canvas.gameObject.AddComponent<CrossPromationWidget>();

            return cpw;
        }

        public const string appsUrl = "https://raw.githubusercontent.com/gracecatgame/applist/master/more_apps.json";
        IEnumerator GetText(Action<AppList> callback)
        {
            UnityWebRequest www = new UnityWebRequest(appsUrl)
            {
                downloadHandler = new DownloadHandlerBuffer()
            };
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                json = www.downloadHandler.text;
                // 将结果显示为文本
                Debug.Log(json);
                var apps = JsonUtility.FromJson<AppList>(json);
                callback?.Invoke(apps);
            }
        }
    }

    public class CrossPromationWidget : UIWidgetsPanel
    {
        public static AppList apps = null;

        protected override Widget createWidget()
        {
#if TEST
            apps = new AppList()
            {
                apps = new List<AppInfo>()
                {
                    new AppInfo()
                    {
                        icon = "https://lh3.googleusercontent.com/dDB1KElDbJTHYeEwpEMFKGV2r6OKPdiu9asU7qAgmLqd2gghIw87rNlY19X7QV0or68=w300",
                        name = "Hexa Blocks - Free Puzzle Game",
                        stars = 5,
                        desc = "Really simple, but not easy at all, Tap as fast as you can.",
                        downloadUrl = "https://play.google.com/store/apps/details?id=com.gracecat.hexablocks"
                    },
                    new AppInfo()
                    {
                        icon = "https://lh3.googleusercontent.com/PVgaG1DhdcN_BjZaAfrcpDdSpim8zZC_mD5L9jt_TjYG1x-2zJljDCIw4G7RrTd2LzCF=w300",
                        name = "Circle Rush",
                        stars = 5,
                        desc = "Really simple, but not easy at all, Tap as fast as you can.",
                        downloadUrl = "https://play.google.com/store/apps/details?id=com.gracecat.hexablocks"
                    }
                }
            };
#endif
            if (null == apps)
            {
                return null;
            }

            int index = UnityEngine.Random.Range(0, apps.apps.Count);
            AppInfo app = apps.apps[index];

            return new WidgetsApp(
                home: new AppWinWidget(this, app),
                pageRouteBuilder: this.pageRouteBuilder);
        }

        protected PageRouteFactory pageRouteBuilder
        {
            get
            {
                return (RouteSettings settings, WidgetBuilder builder) =>
                    new PageRouteBuilder(
                        settings: settings,
                        pageBuilder: (BuildContext context, Animation<float> animation,
                            Animation<float> secondaryAnimation) => builder(context)
                    );
            }
        }

        protected override void OnEnable()
        {
            FontManager.instance.addFont(Resources.Load<Font>(path: "MaterialIcons-Regular"), "Material Icons");
            base.OnEnable();
        }
    }

    public class AppWinWidget : StatefulWidget
    {
        private readonly AppInfo appInfo;
        private readonly Behaviour behaviour;

        public AppWinWidget(Behaviour behaviour, AppInfo info)
        {
            this.behaviour = behaviour;
            this.appInfo = info;
        }

        public override State createState()
        {
            return new AppWinState(this.behaviour, this.appInfo);
        }
    }

    public class AppWinState : State<AppWinWidget>
    {
        private readonly AppInfo appInfo;
        private readonly Behaviour behaviour;

        public AppWinState(Behaviour behaviour, AppInfo info)
        {
            this.behaviour = behaviour;
            this.appInfo = info;
        }

        public override Widget build(BuildContext context)
        {
            return new Center(
                widthFactor: 1.0f,
                heightFactor: 1.0f,
                child: new Container(
                width: 520f,
                height: 520.0f,
                //constraints: BoxConstraints.tight(new Size(520, 520)),
                //padding: EdgeInsets.only(top: 50),
                //alignment: Alignment.center,
                decoration: new BoxDecoration(
                    color: Unity.UIWidgets.ui.Color.white
#if GIZMOS
                    ,border: Border.all(color: Unity.UIWidgets.ui.Color.black, width: 5),
                    borderRadius: BorderRadius.all(20)
#endif
                    ),
                child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    //verticalDirection: VerticalDirection.down,
                    children: new List<Widget>() {
                        new Container(
                            padding: EdgeInsets.only(left: 50, top: 16),
                            decoration: new BoxDecoration(
                                color: Unity.UIWidgets.ui.Color.white
#if GIZMOS
                                ,border: Border.all(color: Unity.UIWidgets.ui.Color.black, width: 5),
                                borderRadius: BorderRadius.all(20)
#endif
                            ),
                            width: 420,
                            height: 520,
                            child: new AppInfoWin(appInfo.icon, appInfo.name, appInfo.desc, appInfo.downloadUrl)
                        ),

                        new Scaffold(
                            body:new IconButton(
                                //padding:EdgeInsets.only(right: 8),
                                icon: new Icon(Icons.clear, size: 64f, color: Colors.grey),
                                        onPressed: () => { this.behaviour.enabled = false; }
                            ))
                    }
                )
            ));
        }
    }

    public class WinTitle : StatelessWidget
    {
        public readonly string title = string.Empty;
        public readonly int fontSize = 30;
        public WinTitle(string title, int size)
        {
            this.title = title;
            this.fontSize = size;
        }

        public override Widget build(BuildContext context)
        {
            return new Container(
                            alignment: Alignment.center,
                            decoration: new BoxDecoration(
                                color: new Unity.UIWidgets.ui.Color(0xFFED6A8A),
                                borderRadius: BorderRadius.all(40)),
                            width: 300,
                            height: 80,
                            child: new Text(data: title, textAlign: TextAlign.justify, style: new TextStyle(color: Colors.white, fontSize: fontSize)));
        }
    }

    public class AppInfoWin : StatelessWidget
    {
        private readonly string iconUrl;
        private readonly string name;
        private readonly string desc;
        private readonly string download;
        public AppInfoWin(string iconUrl, string name, string desc, string download)
        {
            this.iconUrl = iconUrl;
            this.name = name;
            this.desc = desc;
            this.download = download;
        }

        public override Widget build(BuildContext context)
        {
            return new Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            //verticalDirection: VerticalDirection.down,
                            children: new List<Widget>(){
                                            Unity.UIWidgets.widgets.Image.network(this.iconUrl, width: 180, height: 180),
                                            new Text(data: this.name, maxLines: 2, textAlign: TextAlign.center, style: new TextStyle(color: Colors.black, fontSize: 36)),
                                            new Text(data: this.desc, maxLines: 2, textAlign: TextAlign.center, style: new TextStyle(color: Colors.black, fontStyle: Unity.UIWidgets.ui.FontStyle.italic, fontSize: 28)),
                                            new Container(
                                                //padding: EdgeInsets.only(top: 20),
                                                decoration: new BoxDecoration(
                                                    color: Unity.UIWidgets.ui.Color.white
#if GIZMOS
                                                    ,border: Border.all(color: Unity.UIWidgets.ui.Color.black, width: 5),
                                                    borderRadius: BorderRadius.all(20)
#endif
                                                ),
                                                padding: EdgeInsets.only(top: 10, bottom: 20),
                                                child: new CustomButton(
                                                backgroundColor: Unity.UIWidgets.ui.Color.fromARGB(255, 240, 160, 16),
                                                padding: EdgeInsets.symmetric(/*horizontal: 80, */vertical: 20),
                                                child: new Text("Play Now", textAlign: TextAlign.center,style: new TextStyle(
                                                    fontSize: 38, color: Unity.UIWidgets.ui.Color.fromARGB(255, 255, 255, 255), fontWeight: FontWeight.bold
                                                )), onPressed: () => { Application.OpenURL(this.download); }))
                                        }
                            );
        }
    }

    public class InfoDisplayWidget : StatelessWidget
    {
        private readonly string iconUrl;
        private readonly string name;
        private readonly int stars;
        private readonly string download;
        public InfoDisplayWidget(string iconUrl, string name, int stars, string download)
        {
            this.iconUrl = iconUrl;
            this.name = name;
            this.stars = stars;
            this.download = download;
        }

        public override Widget build(BuildContext context)
        {
            return new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            verticalDirection: VerticalDirection.down,
                            children: new List<Widget> {
                                new Container(
                                    decoration: new BoxDecoration(
                                        color: Unity.UIWidgets.ui.Color.white
#if GIZMOS
                                        ,border: Border.all(color: Unity.UIWidgets.ui.Color.black, width: 5),
                                        borderRadius: BorderRadius.all(20)
#endif
                                    ),
                                    width: 220,
                                    height: 220,
                                    child: Unity.UIWidgets.widgets.Image.network(this.iconUrl, fit: BoxFit.fitHeight)
                                ),
                                new Container(
                                    padding: EdgeInsets.only(left: 26),
                                    decoration: new BoxDecoration(
                                        color: Unity.UIWidgets.ui.Color.white
#if GIZMOS
                                        , border: Border.all(color: Unity.UIWidgets.ui.Color.black, width: 5),
                                        borderRadius: BorderRadius.all(20)
#endif
                                    ),
                                    width: 380,
                                    height: 220,
                                    child: new Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        verticalDirection: VerticalDirection.down,
                                        children: new List<Widget>(){
                                            new Text(data: this.name, maxLines: 2, textAlign: TextAlign.left, style: new TextStyle(color: Colors.black, fontSize: 40)),
                                            new StarDisplayWidget(value: this.stars, size: 62),
                                            new CustomButton(
                                                backgroundColor: Unity.UIWidgets.ui.Color.fromARGB(255, 13, 88, 84),
                                                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                                child: new Text("PLAY>", style: new TextStyle(
                                                    fontSize: 30, color: Unity.UIWidgets.ui.Color.fromARGB(255, 255, 255, 255), fontWeight: FontWeight.bold
                                                )), onPressed: () => { Application.OpenURL(this.download); })
                                        })
                                ),
                            });
        }
    }

    public class StarDisplayWidget : StatelessWidget
    {
        public readonly int value = 0;
        private readonly Icon fillStar = null;
        private readonly Icon unfillStar = null;

        public StarDisplayWidget(Key key = null, int value = 0, float size = 16, MaterialColor color = null) : base(key: key)
        {
            this.value = value;
            fillStar = new Icon(Icons.star, size: size, color: color ?? Colors.yellow);
            unfillStar = new Icon(Icons.star_border, size: size, color: color ?? Colors.yellow);
        }

        public override Widget build(BuildContext context)
        {
            return new Row(
              mainAxisSize: MainAxisSize.min,
              children: new List<Widget>() { GetStarWidget(0),
                  GetStarWidget(1),GetStarWidget(2),
                  GetStarWidget(3),GetStarWidget(4),});
        }

        public Widget GetStarWidget(int index)
        {
            return index < this.value ? fillStar : unfillStar;
        }
    }

    public class CustomButton : StatelessWidget
    {
        public CustomButton(
            Key key = null,
            GestureTapCallback onPressed = null,
            EdgeInsets padding = null,
            Unity.UIWidgets.ui.Color backgroundColor = null,
            Widget child = null
        ) : base(key: key)
        {
            this.onPressed = onPressed;
            this.padding = padding ?? EdgeInsets.all(8.0f);
            this.backgroundColor = backgroundColor;
            this.child = child;
        }

        public readonly GestureTapCallback onPressed;
        public readonly EdgeInsets padding;
        public readonly Widget child;
        public readonly Unity.UIWidgets.ui.Color backgroundColor;

        public override Widget build(BuildContext context)
        {
            return new GestureDetector(
                onTap: this.onPressed,
                child: new Container(
                    padding: this.padding,
                    decoration: new BoxDecoration(
                        color: this.backgroundColor,
                        borderRadius: BorderRadius.all(8)),
                    child: this.child
                )
            );
        }
    }

    [Serializable]
    public class AppList
    {
        public List<AppInfo> apps;
    }
    [Serializable]
    public class AppInfo
    {
        public string icon;
        public string name;
        public string desc;
        public int stars;
        public string downloadUrl;
    }
}