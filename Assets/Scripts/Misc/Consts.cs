﻿public class Consts
{
    public const string CompanyName = "Grace Cat";


    // App ID to rate it
    public const string IOSAppId = "1438370178";

    public const string AndroidAppId = "";

    public const string BestScorePrefsKey = "BestScore";

    public const float AdTimeInterval = 25f;


#if UNITY_IOS
    public const string GameName = "Puzzle.Square";
	public const string MoreAppUrl = @"https://itunes.apple.com/cn/developer/jiajia-hu/id1408693825?mt=8";
#elif UNITY_ANDROID
    public const string GameName = "Puzzle Square";
    public const string MoreAppUrl = "https://play.google.com/store/apps/developer?id=Grace+Cat";
#else
    public const string GameName = "Puzzle Square";
	public const string MoreAppUrl = "https://gracecat.github.io/";
#endif

    #region Keystore
#if UNITY_EDITOR
    public readonly static string LowerCompanyName = CompanyName.Replace(" ", string.Empty).ToLower();
    // 某些 iOS app 为了防止重名，会在游戏名中加入一些特殊字符
    public readonly static string LowerGameName = Consts.GameName.Replace(".", string.Empty).Replace(":", string.Empty).Replace(" ", string.Empty).ToLower();
    public readonly static string PackageName = "com.gracecat.square";

    public readonly static string KeystorePath = UnityEngine.Application.dataPath.Replace("Assets", "Keystore");
    public readonly static string KeystoreName = string.Format("{0}/{1}_{2}.keystore", KeystorePath, LowerCompanyName, LowerGameName);
    // 密钥密码，默认是 gracecat
    public readonly static string KeystorePass = "gracecat@2018";

    // 密钥别名，默认是游戏名
    public readonly static string KeyaliasName = "gracecat";
    public readonly static string KeyaliasPass = KeystorePass;
    #endif
    #endregion
}
