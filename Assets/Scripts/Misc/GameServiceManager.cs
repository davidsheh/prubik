﻿using UnityEngine;
using EasyMobile;
using UnityEngine.SocialPlatforms;

public class GameServiceManager : MonoBehaviour
{
    public static GameServiceManager Instance = null;

    void Awake()
    {
        if (null == Instance)
        {
            Instance = this;
        }

        // Init EM runtime if needed (useful in case only this scene is built).
        if (!RuntimeManager.IsInitialized())
            RuntimeManager.Init();
    }

    void Start()
    {
        if (!EM_Settings.GameServices.IsAutoInit)
        {
            Init();
        }
    }

    #region Public

    public void Init()
    {
        if (GameServices.IsInitialized())
        {
            NativeUI.Alert("Alert", "The module is already initialized.");
        }
        else
        {
            GameServices.Init();
        }
    }

    public void ShowLeaderboardUI()
    {
        if (GameServices.IsInitialized())
        {
            GameServices.ShowLeaderboardUI();
        }
        else
        {
#if UNITY_ANDROID
            GameServices.Init();
#elif UNITY_IOS
            NativeUI.Alert("Service Unavailable", "The user is not logged in.");
#else
                Debug.Log("Cannot show leaderboards: platform not supported.");
#endif
        }
    }

    public void ShowAchievementUI()
    {
        if (GameServices.IsInitialized())
        {
            GameServices.ShowAchievementsUI();
        }
        else
        {
#if UNITY_ANDROID
            GameServices.Init();
#elif UNITY_IOS
            NativeUI.Alert("Service Unavailable", "The user is not logged in.");
#else
                Debug.Log("Cannot show achievements: platform not supported.");
#endif
        }
    }

    public void UnlockAchievement(string name)
    {
        if (!GameServices.IsInitialized())
        {
            NativeUI.Alert("Alert", "You need to initialize the module first.");
            return;
        }
        GameServices.UnlockAchievement(name);
    }

    public void ReportScore(int score)
    {
        if (!GameServices.IsInitialized())
        {
            //NativeUI.Alert("Alert", "You need to initialize the module first.");
            return;
        }

        string name = GetLeaderboardName();
        GameServices.ReportScore(score, name);
    }

    public void LoadLocalUserScore(string name = "")
    {
        if (!GameServices.IsInitialized())
        {
            NativeUI.Alert("Alert", "You need to initialize the module first.");
            return;
        }

        if (string.IsNullOrEmpty(name))
        {
            name = GetLeaderboardName();
        }

        GameServices.LoadLocalUserScore(name, OnLocalUserScoreLoaded);
    }

    private string GetLeaderboardName()
    {
        string name = string.Empty;
#if EM_ADMOB
        var leaderboards = EM_Settings.GameServices.Leaderboards;
        if (leaderboards.Length > 0)
        {
            name = leaderboards[0].Name;
        }
#endif

        return name;
    }

    public void LoadFriends()
    {
        if (!GameServices.IsInitialized())
        {
            NativeUI.Alert("Alert", "You need to initialize the module first.");
            return;
        }

        GameServices.LoadFriends(OnFriendsLoaded);
    }

    public void SignOut()
    {
        GameServices.SignOut();
    }

    public void ShowMoreGame()
    {
        Application.OpenURL(Consts.MoreAppUrl);
    }

    public void RateApp()
    {
        if (StoreReview.CanRequestRating())
            StoreReview.RequestRating();
        else
            NativeUI.Alert("Alert", "The rating popup could not be shown because it was disabled or the display constraints are not satisfied.");
    }

    public void RequestRatingLocalized()
    {
        if (!StoreReview.CanRequestRating())
        {
            NativeUI.Alert("Alert", "The rating popup could not be shown because it was disabled or the display constraints are not satisfied.");
            return;
        }

        // For demo purpose, we translated the default content into French using Google Translate!
        var localized = new RatingDialogContent(
                            "Évaluation " + RatingDialogContent.PRODUCT_NAME_PLACEHOLDER,
                            "Comment évalueriez-vous " + RatingDialogContent.PRODUCT_NAME_PLACEHOLDER + "?",
                            "C'est mauvais. Souhaitez-vous nous donner quelques commentaires à la place?",
                            "Impressionnant! Faisons le!",
                            "Pas maintenant",
                            "Non",
                            "Évaluez maintenant!",
                            "Annuler",
                            "Réaction"
                        );


        StoreReview.RequestRating(localized);
    }
    #endregion

    #region Private

    void OnLocalUserScoreLoaded(string leaderboardName, IScore score)
    {
        // if (score != null)
        // {
        //     NativeUI.Alert("Local User Score Loaded", "Your score on leaderboard \"" + leaderboardName + "\" is " + score.value);
        // }
        // else
        // {
        //     NativeUI.Alert("Local User Score Load Failed", "You don't have any score reported to leaderboard \"" + leaderboardName + "\".");
        // }

        // TODO: 同步最高分到本地
    }

    void OnFriendsLoaded(IUserProfile[] friends)
    {
        // if (friends.Length > 0)
        // {
        //     var items = new Dictionary<string, string>();

        //     foreach (IUserProfile user in friends)
        //     {
        //         items.Add(user.userName, user.id);
        //     }

        //     ScrollableList.Create(scrollableListPrefab, "FRIEND LIST", items);
        // }
        // else
        // {
        //     NativeUI.Alert("Load Friends Result", "Couldn't find any friend.");
        // }
    }

    #endregion

    #region Saved Games Demo

    public void OpenSavedGamesDemo()
    {
        // demoUtils.GameServiceDemo_SavedGames();
    }

    #endregion // Saved Games
}